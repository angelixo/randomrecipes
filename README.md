# Random Recipes

100days100projects - An easy tool to get recipes, just press a button and get a random recipe!

It works thanks to [TheMealDB](https://www.themealdb.com/)❤️. 

DEMO at 🍔🍔[https://randomrecipts.web.app/](https://randomrecipts.web.app/)


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
